use seed::{prelude::*, *};
use serde::{Deserialize, Serialize};

// ------ ------
//     Model
// ------ ------
#[derive(Default, Serialize, Deserialize)]
pub struct Model {
    principal_input: String,
    rate_input: String,
    term_input: String,
}

impl Model {
    pub fn new() -> Self {
        Model {
            principal_input: "100000".to_string(),
            rate_input: "5".to_string(),
            term_input: "30".to_string(),
        }
    }
}

// ------ ------
//     Update
// ------ ------

pub enum OutMsg {
    NoOp,
    RemoveMe,
}

#[derive(Clone)]
pub enum Msg {
    PrincipalInput(String),
    RateInput(String),
    TermInput(String),
    RemoveClicked,
}

pub fn update(msg: Msg, model: &mut Model) -> OutMsg {
    let out_msg = match msg {
        Msg::RemoveClicked => OutMsg::RemoveMe,
        _ => OutMsg::NoOp,
    };
    match msg {
        Msg::PrincipalInput(inp) => model.principal_input = inp,
        Msg::RateInput(inp) => model.rate_input = inp,
        Msg::TermInput(inp) => model.term_input = inp,
        Msg::RemoveClicked => (),
    };
    out_msg
}

// ------ ------
//     View
// ------ ------

pub fn view(model: &Model) -> Node<Msg> {
    let monthly_payment =
        view_monthly_payment(&model.principal_input, &model.rate_input, &model.term_input);
    div![
        style![St::Border => "1px solid black",
               St::Padding => unit!(1, rem),
               St::Position => "relative"
               St::MarginBottom => unit!(1, rem),
        ],
        table![tbody![
            input_row(
                "Loan Ammount",
                input![
                    attrs![
                    At::Type => "number",
                    At::Step => "10000",
                    At::Value => model.principal_input
                    ],
                    input_ev(Ev::Input, Msg::PrincipalInput),
                ]
            ),
            input_row(
                "Interest Rate",
                div![
                    input![
                        attrs![
                        At::Type => "number",
                        At::Step => ".01",
                        At::Value => model.rate_input
                        ],
                        input_ev(Ev::Input, Msg::RateInput),
                    ],
                    "%"
                ]
            ),
            input_row(
                "Loan Term",
                div![
                    input![
                        attrs![
                        At::Type => "number",
                        At::Step => "1",
                        At::Value => model.term_input
                        ],
                        input_ev(Ev::Input, Msg::TermInput),
                    ],
                    "years"
                ]
            ),
        ]],
        p![
            "Monthly Payment: ",
            strong![monthly_payment.unwrap_or_else(|| "Input Error!".to_string())]
        ],
        button![
            style![St::Position => "absolute", St::Top => unit!(1, rem), St::Right => unit!(1, rem)],
            simple_ev(Ev::Click, Msg::RemoveClicked),
            "-"
        ]
    ]
}

fn input_row(title: &str, content: Node<Msg>) -> Node<Msg> {
    tr![td![title], td![content]]
}

fn view_monthly_payment(
    principal_input: &str,
    rate_input: &str,
    term_input: &str,
) -> Option<String> {
    let rate: f64 = rate_input.parse().ok()?;
    let principal: f64 = principal_input.parse().ok()?;
    let term: f64 = term_input.parse().ok()?;
    let payment = calc_monthly_payment(principal, rate / 100.0, term);
    Some(format!("{:.2}", payment))
}

// ------ ------
//     Calc
// ------ ------

fn calc_monthly_payment(principal: f64, rate: f64, term: f64) -> f64 {
    let years = term;
    let monthly_rate = rate / 12.0;
    let p = principal;
    let r = monthly_rate;
    let n = years * 12.0;
    let monthly = p * ((r * (1.0 + r).powf(n)) / (((1.0 + r).powf(n)) - 1.0));
    round_to_cents(monthly)
}

fn round_to_cents(value: f64) -> f64 {
    (value * 100.0).round() / 100.0
}
